# import only system from os
from os import system, name


class Juros_Compostos:

	#Constantes
	inflacao = 0.29
	inp_rend = 15


	#Lucro total arecadado
	lucro_total = 0.00

	#Lucro com os descontos
	lucro_real = 0.00

	#Controle para virada de ano.
	cont_ano = 0

	#Valor lucrado em um ano.
	lucro_ano = 0.00


	# define our clear function
	def clear(self):
	    # for windows
	    if name == 'nt':
	        _ = system('cls')

	    # for mac and linux(here, os.name is 'posix')
	    else:
	        _ = system('clear')


	def as_currency(self, amount):
	    if amount >= 0:
	        return 'R$ {:,.2f}'.format(amount)

	    else:
	        return '-R$ {:,.2f}'.format(-amount)


	def menu(self):
		self.clear()
		print("---------------------------")
		print("| 1° Valor Inicial:       |")
		print("| 2° Acrescimo Mensal:    |")
		print("| 3° Meses:               |")
		print("| 4° Rentabilidade %:     |")
		print("---------------------------")
		print("")

		print("Valor Inicial:")
		valor = real = float(input());

		print("Acrescimo Mensal:")
		mensal = float(input())

		print("Meses:")
		meses = int(input())

		print("Rentabilidade %:")
		porcem = float(input())
		print("")

		return valor, real, mensal, meses, porcem


	def calcular_lucros(self):
		valor, real, mensal, meses, porcem = self.menu()

		for i in range(0, meses):
			self.cont_ano += 1

			valor += mensal
			real += mensal

			lucro = valor * porcem / 100
			self.lucro_total += lucro
			self.lucro_ano += lucro

			valor += lucro
			real += lucro
			
			#I.R. - A.M
			#real = valor - (lucro * inp_rend / 100)

			#I.R. - A.A.
			if self.cont_ano == 12:
				self.cont_ano = 0

				#I.R. - A.A.
				real = valor - (self.lucro_ano * self.inp_rend / 100)

				self.lucro_ano = 0.00
			
			#I.R - A.P
			elif meses < 12 and i == meses-1:
				#I.R. - A.P.
				real = valor - (self.lucro_total * self.inp_rend / 100)

			real = real - (real * self.inflacao / 100)

			print(f"| Mês: {i}: {self.as_currency(valor)}          | Real: {self.as_currency(real)}")

		print("")
		print(f"Diferença (Valor - Real): {self.as_currency(valor - real)}          | Lucro Total: {self.as_currency(self.lucro_total)}")
		print("")
		print(f"Real: (Inflação {self.inflacao}% a.m. + I.R. {self.inp_rend}% a.a.)")

		self.deseja_sair()


	def deseja_sair(self):
		print("")
		print("Calcular novamente? (S/n)")
		sair = input()

		if sair == "S" or sair == "s":
			Juros_Compostos().calcular_lucros()


Juros_Compostos().calcular_lucros()